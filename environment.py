import numpy as np


def object_list_to_dictionnary_list(object_list):
    return [object.get_features() for object in object_list]

def proba_talk(rng):
    p = rng.rand()
    talk = 1
    if (p < 0.1):
        talk = 0
    elif (p > 0.9):
        talk = 2
    return talk

#% use 2 different generators to keep a good acceptance rate
def proba_animal_trip(rng):
    p = rng.rand()
    animal = 0
    if (p < 0.3):
        animal = 1
    return animal

def proba_animal_user(rng):
    p = rng.rand()
    animal = 0
    if (p < 0.05):
        animal = 1
    return animal


class Trip:
    """Basic class for our environment item."""

    def __init__(self, _rng, mean_riding_distance=50., mean_walk_distance=1.):
        """
        mean_riding_distance : The average of the riding distance used.
        mean_walk_distance : The average of the walking distance used.
        This class is a struct, it has 3 attributes
        riding_distance : the distance of the ride, generated using a normal centered on the given mean
        walking_distance : the distance of the walk, generated using a normal centered on the given mean
        price : the price of the trip, generated with the riding distance multiplied by random parameter (driver's choice)
        """
        self.riding_distance = _rng.normal(mean_riding_distance, mean_riding_distance / 100.)
        self.walking_distance = _rng.normal(mean_walk_distance, mean_walk_distance / 100.)
        self.price = self.riding_distance * _rng.normal(1, 0.01)
        self.notation = _rng.normal(4, 0.5)
        self.talk = proba_talk(_rng) # 0 : quiet, 1 : little chat, 2: talkative
        self.animal = proba_animal_trip(_rng) # 0 : animal non accepted, 1 : animal accepted
        self.random_features = [_rng.random() for i in range(10)]
    
    def get_features(self):
        out = {}
        for feature in self.__dict__:
            try:
                for i, val in enumerate(list(self.__dict__[feature])):
                    out[feature + '_' + str(i)] = val
            except:
                out[feature] = self.__dict__[feature]
        return out


class User:
    """Basic class for our environment user."""

    def __init__(self, _rng, mean_riding_distance=49., mean_walk_distance=.5):
        """
        mean_riding_distance : The average of the riding distance used.
        mean_walk_distance : The average of the walking distance used.
        This class has 2 attributes
        max_walking_pref : the maximum acceptable distance of the walk, generated using a normal centered on the given mean
        max_price_preference : the maximum acceptable price of the trip, generated with the mean riding distance multiplied by random parameter
        """
        self.max_walking_pref = _rng.normal(
            mean_walk_distance, mean_walk_distance / 100.)
        self.max_price_preference = mean_riding_distance * _rng.normal(1, 0.01)
        self.min_notation = _rng.normal(3, 0.05)
        self.talk = proba_talk(_rng)
        self.animal = proba_animal_user(_rng) #0 : no animal, 1 : user with animal

    def accepts_trip(self, trip):
        """Decides wether this user accepts a given trip"""
        return self.max_price_preference < trip.price and self.max_walking_pref < trip.walking_distance and self.min_notation < trip.notation and (trip.animal == 1 or (trip.animal ==0 and self.animal == 0)) and ((trip.talk == self.talk) or trip.talk == 1 or self.talk == 1)


class EnvBlabla:
    """The full environment class."""
    def __init__(self, nb_users, nb_trips, seed=None):
        self.nb_users = nb_users
        self.nb_trips = nb_trips
        self._seed = seed

    def step(self, action):
        """Plays a step. Action has to be a valid choice."""
        # % Check if action is valid
        if action > len(self.trips) or action < 0:
            raise Exception('Invalid action. action should be a trip index. Got action = {}, valid trips index are from 0 to {}'.format(action, len(self.trips)))

        # % Compute rewards
        potential_rewards = [self.compute_reward(self.user, trip) for trip in self.trips]
        obtained_reward = potential_rewards[action]
        optimal_reward = max(potential_rewards)
        next_state = self.build_next_state()
        return obtained_reward, optimal_reward, next_state

    def reset(self):
        """
        Resets the environment.
        returns the first state
        users : generated users
        _rng : rng that should be used for any random process
        """
        self._rng = np.random.RandomState(self._seed)
        self.users = [User(self._rng) for user_index in range(self.nb_users)]
        first_state = self.build_next_state()
        return first_state

    def build_next_state(self):
        """
        Builds a new state.
        returns the next state as a list of trips as dictionnaries.
        user : user selected
        trips : trips generated
        """
        # % Default values are used for distances
        self.trips = [Trip(self._rng) for trip_index in range(self.nb_trips)]
        self.user_id = self._rng.randint(self.nb_users)
        self.user = self.users[self.user_id]
        return {'user': self.user_id, 'trips' : object_list_to_dictionnary_list(self.trips)}

    def compute_reward(self, user, trip):
        """Computes a reward for a given user and trip"""
        # % Check if the user accepts the trip
        if user.accepts_trip(trip):
            # % 0.05 is the comission
            return 0.05 * trip.price
        else:
            return 0
