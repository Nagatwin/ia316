# IA316

Repo projet IA 316 Océane FOURQUET et Guillaume GOYON

## Idée principale
Notre système de recommandation se base sur celui de Blablacar, qui consiste à proposer des trajets similaires à celui demandé par le client, tout en mettant en avant ceux qui pourrait le plus lui convenir.

## Reward et paramètres à prendre en compte
Le gain de Blablacar se fait sur la commission lors de l'enregistrement d'un trajet. Ce gain est proportionnel au prix du trajet choisi. Blablacar souhaite donc maximiser ce gain.
Pour cela, il faut arriver à ce qu'un maximum de personnes voyagent gràce à l'application. Chaque personne a ses préférences, qui dépendent de plusieurs facteurs: le prix du trajet, les points de départs et d'arrivées, les horaires, les notes des conducteurs, ...

## Agents

Les agents proposés sont tous avec historique. Le code correspondant est dans [./agents.py](./agents.py).

Il y a:

* Un agent Random
* Un agent Epsilon-greedy (inadapté)
* Un agent Random avec historique
* Un agent base sur une estimation de reward par Régression Linéaire sur les items
* Un agent Régression Linéaire avec les user OneHotEncodés
* Un agent de Deep Learning sur les items
* Un agent de Deep Learning avec embedding des users et features des items
* Un agent de Régression Linéaire avec Online Learning



## Remarques

Les agents ont étés sommairement évalués, l'environnement est plutôt lent et inadapté à des ordinateurs de bureau.

Les paramètres des agents de Deep Learning (nombre d'étages, nombre de cellules) dépendent beaucoup de l'environnement et sont donc plutôt arbitraires (déterminés empiriquement, sans cross-validation).

Quelques études en plus ont étés menées (steps de l'historique, comportement de l'environnement).