import numpy as np

def run_exp(agent, env, nb_steps):
    #% Init tracking values
    rewards = np.zeros(nb_steps)
    regrets = np.zeros(nb_steps)
    actions = np.zeros(nb_steps, dtype=int)
    
    #% First context
    context = env.reset()
    
    for i in range(nb_steps):
        # Select action from agent policy.
        action = agent.act(context)
        
        # Play action in the environment and get reward.
        reward, optimal_reward , next_context = env.step(action)
        
        # Update agent.
        agent.update(context, action, reward)
        context = next_context
        
        # Save history.
        rewards[i] = reward
        actions[i] = action
        regrets[i] = optimal_reward - reward
    
    reward = rewards.sum()
    regret = np.sum(regrets)
    return {'reward': reward, 
            'regret': regret,
            'rewards': rewards,
            'regrets': regrets,
            'actions': actions,
            'cum_rewards': np.cumsum(rewards), 
            'cum_regrets': np.cumsum(regrets)
            }

def run_exp_with_hist(agent_build_hist, agent_use_hist, env, nb_steps, nb_steps_hist):
    #% Init tracking values
    rewards = np.zeros(nb_steps)
    regrets = np.zeros(nb_steps)
    actions = np.zeros(nb_steps, dtype=int)
    contexts = []
    
    #% First context
    context = env.reset()
    
    for i in range(nb_steps_hist):
        contexts += [context]
        # Select action from agent policy.
        action = agent_build_hist.act(context) #% random hist build
        
        # Play action in the environment and get reward.
        reward, optimal_reward , next_context = env.step(action)
        
        # Update agent.
        agent_build_hist.update(context, action, reward)
        context = next_context
        
        # Save history.
        rewards[i] = reward
        actions[i] = action
        regrets[i] = optimal_reward - reward
    
    #% Update 2nd agent
    agent_use_hist.build_from_hist(rewards[:nb_steps_hist], actions[:nb_steps_hist], contexts[:nb_steps_hist])
    
    for i in range(nb_steps_hist, nb_steps):
        contexts += [context]
        # Select action from agent policy.
        action = agent_use_hist.act(context)
        
        # Play action in the environment and get reward.
        reward, optimal_reward , next_context = env.step(action)
        
        # Update agent.
        agent_use_hist.update(context, action, reward)
        context = next_context
        
        # Save history.
        rewards[i] = reward
        actions[i] = action
        regrets[i] = optimal_reward - reward
    
    reward = rewards.sum()
    regret = np.sum(regrets)
    return {'reward': reward, 
            'regret': regret,
            'rewards': rewards,
            'regrets': regrets,
            'actions': actions,
            'cum_rewards': np.cumsum(rewards), 
            'cum_regrets': np.cumsum(regrets)
            }