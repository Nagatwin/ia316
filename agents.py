import numpy as np

from sklearn.linear_model import LinearRegression

from keras.layers import Embedding, Flatten, Dense, Concatenate
from keras.models import Model, Sequential
from keras.callbacks import EarlyStopping
import keras.backend as K


class RandomAgent:
    """ Random agent. """
    def __init__(self, seed=None):
        self._rng = np.random.RandomState(seed)

    def act(self, context):
        """ Random action. """
        return self._rng.randint(len(context))

    def update(self, context, action, reward):
        """ No update """
        pass

#% Will not work because context-related features are not taken into account and context is fully generated (context[0] changes between iterations)
def random_argmax(rng, list_):
    """ similar to np.argmax but return a random element among max
        when multiple max exists."""
    return rng.choice(np.argwhere(list_ == list_.max()).flatten())


class EpsilonGreedy:
    """ Epsilon greedy agent. """
    def __init__(self, context, epsilon=0, seed=None):
        self._epsilon = epsilon
        self._rng = np.random.RandomState(seed)
        self._q = np.zeros(len(context))
        self._n = np.zeros(len(context))

    def act(self, context):
        if self._rng.rand() < self._epsilon:
            action = self._rng.randint(len(context))
        else:
            action = random_argmax(self._rng, self._q)
        return action

    def update(self,context, action, reward):
        self._n[action] += 1
        self._q[action] += (reward - self._q[action])/self._n[action]


class RandomAgentWithHist:
    """ Random agent using an history (useless, but has the function build_from_hist). """
    def __init__(self, seed=None):
        self._rng = np.random.RandomState(seed)

    def act(self, context):
        """ Random action. """
        return self._rng.randint(len(context))

    def update(self, context, action, reward):
        """ No update """
        pass

    def build_from_hist(self, rewards, actions, contexts):
        pass

class LinearRegressionAgent:
    """ Linear regression agent from an history. """
    def __init__(self, seed=None):
        self._rng = np.random.RandomState(seed)

    def act(self, context):
        """ Pick the best context based on a model prediction """
        nb_trips = len(context['trips'])
        
        #% Length of one trip
        one_trip_length = len(context['trips'][0])

        #% Build matrix of trips
        X = np.zeros((nb_trips, one_trip_length))
        for trip_index in range(nb_trips):
            trip = context['trips'][trip_index]
            X[trip_index, :] = np.array(list(trip.values()))

        #% Predict trip rewards
        predictions = self.model.predict(X)

        #% Pick the one with the best reward
        action = np.argmax(predictions)
        return action

    def update(self, context, action, reward):
        """ No update for the offline version """
        pass

    def build_from_hist(self, rewards, actions, contexts):
        """ Build the agents parameters with an history. """
        #% Number of steps from the history
        hist_steps = len(contexts)
        one_trip_length = len(contexts[0]['trips'][0])

        #% Features for the model
        X = np.zeros((hist_steps, one_trip_length))
        Y = np.zeros(hist_steps)

        #% Build matrices from history
        for step in range(hist_steps):
            trip_used = contexts[step]['trips'][actions[step]]
            X[step, :] = np.array(list(trip_used.values()))
            Y[step] = rewards[step]

        #% Fit the model
        self.model = LinearRegression(n_jobs=-1)
        self.model.fit(X, Y)
        return



class LinearRegressionAgentOhe:
    """ Linear regression agent from an history. This one uses users One Hot Encoding """
    def __init__(self, seed=None):
        self._rng = np.random.RandomState(seed)

    def act(self, context):
        """ Pick the best context based on a model prediction """
        #% User for this step
        user = context['user']
        nb_trips = len(context['trips'])
        #% Length of one trip
        one_trip_length = len(context['trips'][0])

        #% Build matrix of trips
        X = np.zeros((nb_trips, one_trip_length))
        for trip_index in range(nb_trips):
            trip = context['trips'][trip_index]
            X[trip_index, :] = np.array(list(trip.values()))
        
        #% Find the correct one hot encoding for the user
        user_ohe = np.zeros((nb_trips, len(self.user_table)))
        if user in self.user_table.keys():
            user_ohe[:,self.user_table[user]] = np.ones(nb_trips)
        
        #% An unseen user will have a ohe of 0
        
        X = np.concatenate((X, user_ohe), axis=1)
        
        #% Predict trip rewards
        predictions = self.model.predict(X)

        return np.argmax(predictions)

    def update(self, context, action, reward):
        pass

    def build_from_hist(self, rewards, actions, contexts):
        """ Build the agents parameters with an history. """
        #% Number of steps from the history
        hist_steps = len(contexts)
        one_trip_length = len(contexts[0]['trips'][0])

        #% Features for the model
        X = np.zeros((hist_steps, one_trip_length))
        Y = np.zeros(hist_steps)
        
        #% Users seen in history
        users_used = np.zeros(hist_steps, dtype=int)
        
        #% Table for user - index corresponding in the ohe space
        self.user_table = {}
        
        #% Build matrices from history
        for step in range(hist_steps):
            trip_used = contexts[step]['trips'][actions[step]]
            X[step, :] = np.array(list(trip_used.values()))
            Y[step] = rewards[step]
            
            users_used[step] = contexts[step]['user']
            
            #% Add unseen users in the history
            if not users_used[step] in self.user_table.keys():
                self.user_table[users_used[step]] = len(self.user_table.keys())
        
        users_used = np.array([self.user_table[user] for user in users_used])
        
        #% TODO We should not use the user's ohe the fist time we see him to get a better model
        X = np.concatenate((X, np.eye(len(self.user_table.keys()))[users_used]), axis=1)
        
        #% Fit the model
        self.model = LinearRegression(n_jobs=-1)
        self.model.fit(X, Y)
        return


class LinearRegressionAgentOheOnline:
    """ Linear regression agent from an history. This one uses users One Hot Encoding and online learning """
    def __init__(self, seed=None):
        self._rng = np.random.RandomState(seed)

    def act(self, context):
        """ Pick the best context based on a model prediction """
        #% User for this step
        user = context['user']
        nb_trips = len(context['trips'])
        
        #% Length of one trip
        one_trip_length = len(context['trips'][0])

        #% Build matrix of trips
        X = np.zeros((nb_trips, one_trip_length))
        for trip_index in range(nb_trips):
            trip = context['trips'][trip_index]
            X[trip_index, :] = np.array(list(trip.values()))
        
        user_ohe = np.zeros((nb_trips, len(self.user_table)))
        if user in self.user_table.keys():
            user_ohe[:,self.user_table[user]] = np.ones(nb_trips)
        X = np.concatenate((X, user_ohe), axis=1)
        #% Predict trip rewards
        predictions = self.model.predict(X)

        action = np.argmax(predictions)
        return action
    
    def fit_model(self):
        """ Fits the internal model using internal history """
        self.model.fit(self.X, self.Y)
        self.steps_since_last_fit = 0
        self.user_table = self.next_user_table.copy()
        return

    def update(self, context, action, reward):
        """ Updates the internal history """
        user = context['user']
        trip = np.array(list(context['trips'][action].values()))
        
        if not user in self.next_user_table.keys():
            self.next_user_table[user] = len(self.next_user_table.keys())
            self.X = np.concatenate((self.X, np.zeros((self.X.shape[0], 1))), axis=1)
            
        user_vect = np.eye(len(self.next_user_table.keys()))[self.next_user_table[user]]
        new_entry = np.concatenate((trip.reshape(1, -1), user_vect.reshape(1, -1)), axis=1)
        
        self.X = np.concatenate((self.X, new_entry), axis=0)
        self.Y = np.concatenate((self.Y, [reward]), axis=0)
        
        #% Check for re-fit
        self.steps_since_last_fit += 1
        if self.steps_since_last_fit > 10:
            self.fit_model()
        return

    def build_from_hist(self, rewards, actions, contexts):
        """ Build the agents parameters with an history. """
        #% Number of steps from the history
        hist_steps = len(contexts)
        one_trip_length = len(contexts[0]['trips'][0])

        #% Features for the model
        self.X = np.zeros((hist_steps, one_trip_length))
        self.Y = np.zeros(hist_steps)

        users_used = np.zeros(hist_steps, dtype=int)
        self.next_user_table = {}
        #% Build matrices from history
        for step in range(hist_steps):
            trip_used = contexts[step]['trips'][actions[step]]
            self.X[step, :] = np.array(list(trip_used.values()))
            self.Y[step] = rewards[step]
            users_used[step] = contexts[step]['user']
            if not users_used[step] in self.next_user_table.keys():
                self.next_user_table[users_used[step]] = len(self.next_user_table.keys())
        users_used = np.array([self.next_user_table[user] for user in users_used])
        self.X = np.concatenate((self.X, np.eye(len(self.next_user_table.keys()))[users_used]), axis=1)
        
        #% Fit the model
        self.model = LinearRegression(n_jobs=-1)
        self.fit_model()
        return


class RegressionModel(Model):
    def __init__(self, embedding_size, max_user_id):
        super().__init__()
        
        self.user_embedding = Embedding(output_dim=embedding_size,
                                        input_dim=max_user_id + 1,
                                        input_length=1,
                                        name='user_embedding')
        
        # The following two layers don't have parameters.
        self.flatten = Flatten()
        self.concat = Concatenate()
        self.dense1 = Dense(256, activation='relu')
        self.dense2 = Dense(256, activation='relu')
        self.dense3 = Dense(256, activation='relu')
        self.dense4 = Dense(256, activation='relu')
        self.dense5 = Dense(256, activation='relu')
        self.dense6 = Dense(256, activation='relu')
        self.dense7 = Dense(1, activation='linear')
        
    def call(self, inputs):
        user_inputs = inputs[0]
        item_inputs = inputs[1]
        
        user_vecs = self.flatten(self.user_embedding(user_inputs))
        item_vecs = item_inputs
        
        input_vecs = self.concat([user_vecs, item_vecs])
        
        y = self.dense1(input_vecs)
        y = self.dense2(y)
        y = self.dense3(y)
        y = self.dense4(y)
        y = self.dense5(y)
        y = self.dense6(y)
        y = self.dense7(y)
        
        return y

class DeepLearningAgentWithEmbedding:
    """ Deep Learning agent from an history. """
    def __init__(self, embedding_size, max_user_id, seed=None):
        self.embedding_size = embedding_size
        self.max_user_id = max_user_id
        self._rng = np.random.RandomState(seed)
        
    def act(self, context):
        """ Pick the best context based on a model prediction """
        #% User for this step
        user = context['user']
        self_user = self.user_table[user] if user in self.user_table.keys() else 0
        nb_trips = len(context['trips'])
        #% Length of one trip
        one_trip_length = len(context['trips'][0])

        #% Build matrix of trips
        X = np.zeros((nb_trips, one_trip_length))
        for trip_index in range(nb_trips):
            trip = context['trips'][trip_index]
            X[trip_index, :] = np.array(list(trip.values()))

        #% Predict trip rewards
        predictions = self.model.predict([self_user * np.ones(len(X)), X])
        
        action = np.argmax(predictions)
        return action
        
    def update(self, context, action, reward):
        pass
    
    def build_from_hist(self, rewards, actions, contexts):
        """ Build the agents parameters with an history. """
        #% Number of steps from the history
        hist_steps = len(contexts)
        one_trip_length = len(contexts[0]['trips'][0])

        #% Features for the model
        X = np.zeros((hist_steps, one_trip_length))
        users = np.zeros((hist_steps, 1))
        Y = np.zeros(hist_steps)
        self.user_table = {}

        #% Build matrices from history
        for step in range(hist_steps):
            trip_used = contexts[step]['trips'][actions[step]]
            X[step, :] = np.array(list(trip_used.values()))
            Y[step] = rewards[step]
            
            #% Add user to known users
            user_used = contexts[step]['user']
            if not user_used in self.user_table.keys():
                self.user_table[user_used] = len(self.user_table.keys()) + 1
            
            users[step, 0] = self.user_table[user_used]
        
        #% Fit the model
        self.model = RegressionModel(self.embedding_size, len(self.user_table.keys()))
        early_stopping = EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=True)
        self.model.compile(optimizer="adam", loss='mae')
        self.model.fit([users, X], Y, epochs=10000, validation_split=0.2, callbacks=[early_stopping], shuffle=True, verbose = 0, batch_size = 256)
        return


class DeepLearningAgentWithoutEmbedding:
    """ Deep Learning agent from an history. """
    def __init__(self, seed=None, layers_cells=[256, 128, 64]):
        self.layers_cells = layers_cells
        self._rng = np.random.RandomState(seed)
        
    def act(self, context):
        """ Pick the best context based on a model prediction """
        #% User for this step
        user = context['user']
        nb_trips = len(context['trips'])
        #% Length of one trip
        one_trip_length = len(context['trips'][0])

        #% Build matrix of trips
        X = np.zeros((nb_trips, one_trip_length))
        for trip_index in range(nb_trips):
            trip = context['trips'][trip_index]
            X[trip_index, :] = np.array(list(trip.values()))

        #% Predict trip rewards
        predictions = self.model.predict(X)
        
        action = np.argmax(predictions)
        return action
        
    def update(self, context, action, reward):
        pass
    
    def build_from_hist(self, rewards, actions, contexts):
        """ Build the agents parameters with an history. """
        #% Number of steps from the history
        hist_steps = len(contexts)
        one_trip_length = len(contexts[0]['trips'][0])

        #% Features for the model
        X = np.zeros((hist_steps, one_trip_length))
        Y = np.zeros(hist_steps)

        #% Build matrices from history
        for step in range(hist_steps):
            trip_used = contexts[step]['trips'][actions[step]]
            X[step, :] = np.array(list(trip_used.values()))
            Y[step] = rewards[step]
        
        #% Fit the model
        layers_list = [Dense(self.layers_cells[0], input_shape=(one_trip_length,), activation='relu')]
        if len(self.layers_cells) > 1:
            for cells in self.layers_cells[1:]:
                layers_list += [Dense(cells, activation='relu')]
        layers_list += [Dense(1)]
        self.model = Sequential(layers_list)
        
        early_stopping = EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=True)
        self.model.compile(optimizer="adam", loss='mae')
        self.model.fit(X, Y, epochs=10000, validation_split=0.2, callbacks=[early_stopping], shuffle=True, verbose = 0, batch_size = 16)
        return